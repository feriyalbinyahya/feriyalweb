from django.urls import path
from django.conf.urls import url
from . import views
from django.conf.urls.static import static

app_name = 'profil'

urlpatterns = [
    path('', views.beranda, name="beranda"),
    path('<str:lowongan>', views.render_apply, name="lowongan"),
    path('tentangsaya', views.tentangsaya, name="tentangsaya"),
    path('ceritasaya', views.ceritasaya, name="ceritasaya"),
    path('daftarakun', views.daftarakun, name="daftarakun"),
    path('jadwal', views.jadwal, name='jadwal'),
    path('buatjadwal', views.buatjadwal, name='buatjadwal'),
    path('hapusjadwal', views.hapusjadwal, name='hapusjadwal')
] #+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)