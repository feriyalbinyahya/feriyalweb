from django.shortcuts import render, redirect
from .forms import ScheduleForm
from .models import Jadwal

def render_apply(request, lowongan):
    list_lowongan = ["Data Scientist", "Digital Marketer", "Finance Accounting", "Game Developer", "Jurnalist",
                    "Mobile Developer", "Quality Assurance", "UI/UX Designer", "Web Developer"]
    response = {}
    response['lowongan'] = {'nama_lowongan': list_lowongan[int(lowongan)-1]}
    return render(request, "Pendaftaran.html", response)


def beranda(request):
	return render(request, "Beranda.html")

def tentangsaya(request):
	return render(request, "TentangSaya.html")

def ceritasaya(request):
	return render(request, "CeritaSaya.html")

def daftarakun(request):
	return render(request, "DaftarAkun.html")

def jadwal(request):
    response = {}
    jadwall = Jadwal.objects.all()
    response = {
        "jadwall" : jadwall
    }
    return render(request, 'jadwal.html', response)

data = {}

def buatjadwal(request):
    form = ScheduleForm(request.POST or None)
    response = {}
    if(request.method == "POST"):
        if (form.is_valid()):
            activity = request.POST.get("activity")
            day = request.POST.get("day")
            date = request.POST.get("date")
            time = request.POST.get("time")
            place = request.POST.get("place")
            category = request.POST.get("category")
            Jadwal.objects.create(activity=activity, day=day, date=date, time=time, place=place, category=category)
            return redirect('/jadwal')
        else:
            return render(request, 'JadwalForm.html', response)
    else:
        response['form'] = form
        return render(request, 'JadwalForm.html', response)


def hapusjadwal(request):
    Jadwal.objects.all().delete()
    return redirect('/jadwal')