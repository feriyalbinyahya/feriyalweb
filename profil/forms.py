from django import forms
from .models import Jadwal

class ScheduleForm(forms.Form):
    activity = forms.CharField(label="Nama Kegiatan")
    day = forms.CharField(label="Hari")
    date = forms.DateField(widget=forms.DateInput(attrs={'type' : 'date'}), label="Tanggal")
    time = forms.TimeField(widget=forms.TimeInput(attrs={'type' : 'time'}), label="Jam")
    place = forms.CharField(label="Tempat")
    category = forms.CharField(label="Kategori")